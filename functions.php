<?php

/**
 * Custom amendments for the theme.
 *
 * @category   Genesis_Sandbox
 * @package    Functions
 * @subpackage Functions
 * @author     Travis Smith and Jonathan Perez
 * @license    http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link       http://surefirewebservices.com/
 * @since      1.1.0
 */

// Initialize Sandbox ** DON'T REMOVE **
require_once( get_stylesheet_directory() . '/lib/init.php');

add_action( 'genesis_setup', 'gs_theme_setup', 15 );


// Theme Set Up Function
// Add all the hooks, filters and actions in order here
function gs_theme_setup() {
	
	//Enable HTML5 Support
	add_theme_support( 'html5' );

	//Enable Post Navigation
	add_action( 'genesis_after_entry_content', 'genesis_prev_next_post_nav', 5 );

	/** 
	 * 01 Set width of oEmbed
	 * genesis_content_width() will be applied; Filters the content width based on the user selected layout.
	 *
	 * @see genesis_content_width()
	 * @param integer $default Default width
	 * @param integer $small Small width
	 * @param integer $large Large width
	 */
	$content_width = apply_filters( 'content_width', 600, 430, 920 );
	
	/** 
	 * 02 Structure
	 */
	// Enable Custom Header
	add_theme_support('genesis-custom-header');

	// Add support for structural wraps
	add_theme_support( 'genesis-structural-wraps', array(
		'nav',
		'subnav',
		'inner',
		'footer-widgets',
		'footer'
	) );
	
	//Custom Image Sizes
	add_image_size( 'featured-image', 225, 160, TRUE );
	
	// Enable Custom Background
	//add_theme_support( 'custom-background' );

	/**
	 * 07 Footer Widgets
	 * Add support for 3-column footer widgets
	 * Change 3 for support of up to 6 footer widgets (automatically styled for layout)
	 */
	add_theme_support( 'genesis-footer-widgets', 3 );
	
	/**
	 * 08 Genesis Menus
	 * Genesis Sandbox comes with 4 navigation systems built-in ready.
	 * Delete any menu systems that you do not wish to use.
	 */
	add_theme_support(
		'genesis-menus', 
		array(
			'primary'   => __( 'Primary Navigation Menu', CHILD_DOMAIN ), 
			'secondary' => __( 'Secondary Navigation Menu', CHILD_DOMAIN ),
			'footer'    => __( 'Footer Navigation Menu', CHILD_DOMAIN ),
			'mobile'    => __( 'Mobile Navigation Menu', CHILD_DOMAIN ),
		)
	);
	
	// Add footer menu
	add_action('genesis_before_footer', 'footer_menu');
	
	//* Change the footer credits text
	add_filter('genesis_footer_creds_text', 'sp_footer_creds_filter');

	// Add Mobile Navigation
	add_action('genesis_before', 'gs_mobile_navigation', 5 );
	
	//Enqueue Sandbox Scripts
	add_action('wp_enqueue_scripts', 'gs_enqueue_scripts' );
	
	// Enqueue Addt'l Styles and Scripts
	// add_action( 'wp_enqueue_scripts', 'custom_load_custom_style_sheet' );
	
	/**
	 * 13 Editor Styles
	 * Takes a stylesheet string or an array of stylesheets.
	 * Default: editor-style.css 
	 */
	//add_editor_style();
	
	
	// Register Sidebars
	gs_register_sidebars();
	
	// Actions and filters
	remove_action('genesis_before_loop', 'genesis_do_breadcrumbs'); // remove the breadcrumb
	add_action('genesis_after_header', 'genesis_do_breadcrumbs');   // position it above the content
	add_action('genesis_before_entry', 'reposition_entry_header');  // position the <h1> tag inside main content
	add_filter('genesis_breadcrumb_args', 'sp_breadcrumb_args');    // style the breadcrumb
	add_filter('genesis_home_crumb', 'sp_breadcrumb_home_link');    // change breadcrumb home link
	
	
} // End of Set Up Function

// Register Sidebars
function gs_register_sidebars() {
	$sidebars = array(
		array(
			'id'			=> 'home-top',
			'name'			=> __( 'Home Top', CHILD_DOMAIN ),
			'description'	=> __( 'This is the top homepage section.', CHILD_DOMAIN ),
		), 
		array(
			'id'			=> 'home-middle-01',
			'name'			=> __( 'Home Page Content', CHILD_DOMAIN ),
			'description'	=> __( 'This is the homepage content section.', CHILD_DOMAIN ),
		),
		array(
			'id'			=> 'home-bottom',
			'name'			=> __( 'Home Bottom', CHILD_DOMAIN ),
			'description'	=> __( 'This is the homepage right section.', CHILD_DOMAIN ),
		),
		array(
			'id'			=> 'portfolio',
			'name'			=> __( 'Portfolio', CHILD_DOMAIN ),
			'description'	=> __( 'Use featured posts to showcase your portfolio.', CHILD_DOMAIN ),
		),
		array(
			'id'			=> 'after-post',
			'name'			=> __( 'After Post', CHILD_DOMAIN ),
			'description'	=> __( 'This will show up after every post.', CHILD_DOMAIN ),
		),
		array(
			'id' 			=> 'top-bar-left',
			'name' 			=> __( 'Top Bar Left', CHILD_DOMAIN ),
			'description' 	=> __( 'This is the left utility bar above the header.', CHILD_DOMAIN ),
		),
		array(
			'id' 			=> 'top-bar-right',
			'name' 			=> __( 'Top Bar Right', CHILD_DOMAIN ),
			'description' 	=> __( 'This is the right utility bar above the header.', CHILD_DOMAIN ),
		),
		array(
			'id' 			=> 'site-header',
			'name' 			=> __( 'Site Header', CHILD_DOMAIN ),
			'description' 	=> __( 'This is the main header for the site.', CHILD_DOMAIN ),
		),
		array(
			'id' 			=> 'footer-social',
			'name' 			=> __( 'Footer Social Media', CHILD_DOMAIN ),
			'description' 	=> __( 'This is the div directly below the footer menu.', CHILD_DOMAIN ),
		),
		array(
			'id' 			=> 'footer-content',
			'name' 			=> __( 'Footer Content', CHILD_DOMAIN ),
			'description' 	=> __( 'This is the div directly below the footer social media. It contains the main footer content.', CHILD_DOMAIN ),
		),
	);
	
	foreach ( $sidebars as $sidebar )
		genesis_register_sidebar( $sidebar );
}

function sp_breadcrumb_args( $args ) {
		$args['home'] = '';
		$args['sep'] = ' <span class="delimiter"><i class="x-icon-angle-right"></i></span> ';
		$args['list_sep'] = ', '; // Genesis 1.5 and later
		$args['prefix'] = '<div class="x-breadcrumb-wrap"><div class="x-container-fluid max width cf">';
		$args['suffix'] = '</div></div>';
		$args['heirarchial_attachments'] = true; // Genesis 1.5 and later
		$args['heirarchial_categories'] = true; // Genesis 1.5 and later
		$args['display'] = true;
		$args['labels']['prefix'] = '';
	return $args;
}

function sp_breadcrumb_home_link( $crumb ) {
	//return preg_replace('/href="[^"]*"/', 'href="/home"', $crumb);
	return '<a href="http://sanitred.com"><span class="home"><i class="x-icon-home"></i></span></a>';
}

function custom_load_custom_style_sheet() {
	//wp_enqueue_style( 'custom-stylesheet', CHILD_URL . '/css/integrity-light.css', array(), PARENT_THEME_VERSION );
}
	
function sp_footer_creds_filter( $creds ) {
	$creds = '';
	return $creds;
}
	
function reposition_entry_header() {

	if (!is_home() ) {

		remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
		remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
		remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
		remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );

		add_action( 'genesis_entry_content', 'genesis_do_post_title', 9 );
		add_action( 'genesis_entry_content', 'genesis_post_info', 9 );

	}

}
/**
 * Enqueue and Register Scripts - Twitter Bootstrap, Font-Awesome, and Common.
 */
require_once('lib/scripts.php');

/**
 * Add navigation menu 
 * Required for each registered menu.
 * 
 * @uses gs_navigation() Sandbox Navigation Helper Function in gs-functions.php.
 */

//Add Mobile Menu
function gs_mobile_navigation() {
	
	$mobile_menu_args = array(
		'echo' => true,
	);
	
	gs_navigation( 'mobile', $mobile_menu_args );
}

// Add Widget Area After Post
add_action('genesis_after_entry', 'gs_do_after_entry');
function gs_do_after_entry() {
 	if ( is_single() ) {
 	genesis_widget_area( 
                'after-post', 
                array(
                        'before' => '<aside id="after-post" class="after-post"><div class="home-widget widget-area">', 
                        'after' => '</div></aside><!-- end #home-left -->',
                ) 
        );
 }
 }

// Register Theme Specific widgets
add_action('genesis_before_header', 'utility_bar_logo_header' );
add_action('genesis_footer', 'footer_social_media' );
	


/**
* Add utility bar above header.
*
* @author Carrie Dils
* @copyright Copyright (c) 2013, Carrie Dils
* @license GPL-2.0+
*/
function utility_bar_logo_header() {
 
	echo '<div class="x-topbar"><div class="x-topbar-inner x-container-fluid max width">';
 
 	genesis_widget_area( 'top-bar-right', array(
		'before' => '<div class="p-info">',
		'after' => '</div>',
	) );
	
	genesis_widget_area( 'top-bar-left', array(
		'before' => '<div class="x-social-global">',
		'after' => '</div>',
	) );
 
	echo '</div></div>';	
	echo '<div class="x-logobar"><div class="x-logobar-inner">';
	
	genesis_widget_area( 'site-header', array(
		'before' => '<div class="x-container-fluid max width">',
		'after' => '</div>',
	) );
	
	echo '</div></div>';	
	/*
	echo '<div class="x-container-fluid max width">';
			$args = array(
					'theme_location'  => 'primary',
					'container'       => 'nav',
					'container_class' => 'x-nav-collapse collapse',
					'menu_class'      => 'x-nav sf-menu sf-js-enabled',
					'depth'           => 1,
				);
			wp_nav_menu( $args );
    echo '</div>';
	*/
}

//Footer Menu
function footer_menu () {

}

function footer_social_media() {
 	// Begin the footer container
	echo '<div class="x-colophon-content">';
	
	// Footer Top is the second menu
	echo '<div class="x-container-fluid max width">';
			$args = array(
					'theme_location'  => 'footer',
					'container'       => 'nav',
					'container_class' => 'wrap',
					'menu_class'      => 'x-nav',
					'depth'           => 1,
				);
			wp_nav_menu( $args );
    echo '</div>';
  
    // Social Menu is next
  	echo '<div class="x-social-global-footer">';
		genesis_widget_area( 'footer-social', array(
			'before' => '',
			'after' => '',
		) );
	echo '</div>';
  
    // Footer content is last
	genesis_widget_area( 'footer-content', array(
		'before' => '',
		'after' => '',
	) );
 
	echo '</div>';	
	
}
